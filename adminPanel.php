<?php
session_start();
include_once "connect.php";

if (isset($_POST['logout'])) {
    
    session_destroy();
    header("location: index.php");
}


if (isset($_POST['edit'])) {
    $edit_id = $_POST['edit_id'];
    $edited_data = $_POST['edited_data'];

    if (isset($_GET['table'])) {
        $table = $_GET['table'];
        $update_query = "UPDATE $table SET ";

        
        foreach ($edited_data as $column => $value) {
            $update_query .= "`$column` = '$value', ";
        }
        $update_query = rtrim($update_query, ', '); 
        $update_query .= " WHERE id = '$edit_id'";

        mysqli_query($connect, $update_query);
    }
}

if (isset($_GET['drop'])) {
    $delete_id = $_GET['drop'];

    if (isset($_GET['table'])) {
        $table = $_GET['table'];
        $delete_query = "DELETE FROM $table WHERE id = '$delete_id'";
        mysqli_query($connect, $delete_query);

        header("Location: ?table=$table");
        exit();
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>
    <link rel="stylesheet" href="style.css">
    <style>
    h1 {
        /* margin: 0; */
        font-size: 1.5em;
    }

    form {
        margin-top: 10px;
    }

    button {
        padding: 10px;
        background-color: #3498db;
        color: #fff;
        border: 1px solid #2980b9;
        border-radius: 4px;
        cursor: pointer;
    }

    button:hover {
        background-color: #2980b9;
    }
</style>
</head>

<body>
    <?php
    if (isset($_SESSION['AdminLoginId'])) {
    ?>

        <header>
            <div class="user-info">
                <?php  $adminName = $_SESSION['AdminLoginId'];
                 echo "<h3>Welcome to AdminPanel</h3> "."<h1>$adminName</h1>"; ?>
                <br><br>
                <form method="post">
                    <button name="logout">Log Out</button>
                </form>
            </div>
        </header>
        <main>
            <?php
            $nav_select = "SHOW TABLES";
            $result = mysqli_query($connect, $nav_select);
            $navs = mysqli_fetch_all($result);
            ?>
            <nav>
                <table border="1">
                        <?php foreach ($navs as $menu) { ?>
                            <tr>
                            <td>
                                <h3><a href="?table=<?= $menu[0] ?>"><?= $menu[0] ?></a></h3>
                            </td>
                            </tr>
                        <?php } ?>
                </table>
            </nav>

            <?php
            if (isset($_GET['table'])) {
                $table = $_GET['table'];
                $select = "SELECT * FROM $table";
                $result = mysqli_query($connect, $select);
                $data = mysqli_fetch_all($result);
                $columns = mysqli_fetch_fields($result);
            }
            ?>
            <div class="content">
                <?php if (isset($_GET['table'])) {
                    $tableName = $_GET['table'];
                    echo "<h3>$tableName</h3>";
                } ?>
                <table class="tb">
                    <?php
                    if (isset($_GET['table'])) {
                        $table = $_GET['table'];
                        foreach ($data as $_data) {
                            echo "<tr>";
                            foreach ($_data as $item) {
                                echo "<td>";
                                echo ($item);
                                echo "</td>";
                            }
                            echo "<td>";
                            echo "<a href='?table=$table&drop=$_data[0]'>DELETE</a>";
                            echo "</td>";
                            echo "<td>";
                            echo "<a href='?table=$table&edit=$_data[0]'>Edit</a>";
                            echo "</td>";
                            echo "</tr>";
                        }
                    }
                    ?>
                </table>
                <?php
                if (isset($_GET['edit'])) {
                    $edit_id = $_GET['edit'];
                    $select_edit = "SELECT * FROM $table WHERE id='$edit_id'";
                    $result_edit = mysqli_query($connect, $select_edit);
                    $edit_data = mysqli_fetch_assoc($result_edit);
                ?>
                    <form method="post" class="edit-form">
                        <input type="hidden" name="edit_id" value="<?= $edit_id ?>">
                        <?php
                        foreach ($columns as $column) {
                            $column_name = $column->name;
                            $column_value = $edit_data[$column_name];
                        ?>
                            <label for="<?= $column_name ?>"><?= $column_name ?>:</label>
                            <input type="text" name="edited_data[<?= $column_name ?>]" value="<?= $column_value ?>">
                            <br>
                        <?php
                        }
                        ?>
                        <button name="edit">Save</button>
                    </form>
                <?php } ?>
            </div>
        </main>
    <?php } else {
        echo "something went wrong";
    } ?>
</body>

</html>
