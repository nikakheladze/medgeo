function toggleBackgroundColor() {
    var body = document.body;
    var currentColor = window.getComputedStyle(body).backgroundColor;
    
    // Check the current color and switch to the opposite
    if (currentColor === 'rgb(213, 225, 239)') {
        body.style.backgroundColor = 'rgb(0, 0, 0)';  // Example: switching to black
    } else {
        body.style.backgroundColor = 'rgb(213, 225, 239)';
    }
}

function googleTranslateElementInit() {
    new google.translate.TranslateElement({
      pageLanguage: 'en',
      includedLanguages: 'en,ru,ka', // Add more languages as needed
      layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
      autoDisplay: false
    }, 'google_translate_element');
}

function changeLanguage(language) {
    google.translate.translate(document.body, language);
}